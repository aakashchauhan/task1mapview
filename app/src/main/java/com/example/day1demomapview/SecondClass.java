package com.example.day1demomapview;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SecondClass extends AppCompatActivity implements LocationListener, OnMapReadyCallback {
    Button Btn;
    LocationManager locationManager;
    TextView textView;
    GoogleMap map;
    MapView mapview;
    double lat;
    double longi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondclassxml);
        Btn = findViewById(R.id.button);
        textView = findViewById(R.id.textView);
        mapview = findViewById(R.id.mapview);
        if(ContextCompat.checkSelfPermission(SecondClass.this,Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(SecondClass.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},100);
        }
        if(mapview!=null){
            mapview.onCreate(null);
            mapview.onResume();
            mapview.getMapAsync(this);
        }
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });

    }
    @SuppressLint("MissingPermission")
    private void getLocation() {
        try {
            locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,5,SecondClass.this);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void mapview(View view){
        Fragment fragment;
        fragment = new MapFragmentDemo();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragment,fragment);
        ft.commit();
    }
    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        longi = location.getLongitude();
        textView.setText(lat+" "+longi);
        LatLng currentLoc = new LatLng(lat, longi);
        map.addMarker(new MarkerOptions()
                .position(currentLoc)
                .title("Marker on current location"));
        map.moveCamera(CameraUpdateFactory.newLatLng(currentLoc));
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
    @Override
    public void onProviderEnabled(String provider) {

    }
    @Override
    public void onProviderDisabled(String provider) {

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(SecondClass.this);
        map = googleMap;
    }
}
